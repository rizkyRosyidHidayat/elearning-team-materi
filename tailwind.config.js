module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: false,
  theme: {
    extend: {},
    container: {
      center: true,
      padding: '1rem'
    },
  },
  variants: {},
  plugins: [],
}
