module.exports = {
  configureWebpack: {
    devServer: {
      headers: {
        "Access-Control-Allow-Origin": "*"
      },
      disableHostCheck: true,
      sockPort: 8502,
      sockHost: "localhost"
    },
    externals: ["vue", "vue-router", /^@vue-mf\/.+/, "firebase", "vuex", "single-spa-vue"]
  },
  filenameHashing: false
};
