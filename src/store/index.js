import Vue from 'vue'
import Vuex from 'vuex'
import db from '../config/firebase'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    kuis: [],
    loading: false
  },
  mutations: {
    updateDataKuis (state, payload) {
      state.kuis = payload
    },
    updateLoading(state, payload) {
      state.loading = payload  
    }
  },
  actions: {
    updateLoading (ctx, payload) {
      ctx.commit('updateLoading', payload)
    },
    async getDataKuisByMateri (ctx, payload) {
      ctx.dispatch('updateLoading', true)
      try {
        await db.collection('kuis').where('id_materi', '==', payload).get()
          .then(snapshot => {
            let data = []
            snapshot.docs.map((doc, i) => {
              data[i] = {...doc.data(), id: doc.id}
            })
            ctx.commit('updateDataKuis', data[0].pertanyaan)
          })
      } catch (e) {
        return Promise.reject(e)
      } finally {
        ctx.dispatch('updateLoading', false)
      }
    },
  }
})
