import Vue from 'vue'
import VueRouter from 'vue-router'
import Materi from '../views/Materi'
import Quiz from '../views/Quiz'

Vue.use(VueRouter)

const routes = [
  {
    path: '/materi/:id',
    name: 'materi',
    component: Materi,
    props: true
  },
  {
    path: '/kuis/:id',
    name: 'kuis',
    component: Quiz,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
